Source: python-libnmap
Section: python
Priority: optional
Maintainer: Sophie Brun <sophie@freexian.com>
Build-Depends: debhelper (>= 10), dh-python, python-all, python-setuptools, python3-all, python3-setuptools
Standards-Version: 3.9.8
Homepage: https://pypi.python.org/pypi/python-libnmap
X-Python-Version: >= 2.6
X-Python3-Version: >= 3.3
Vcs-Git: git://git.kali.org/packages/python-libnmap.git
Vcs-Browser: http://git.kali.org/gitweb/?p=packages/python-libnmap.git;a=summary
Testsuite: autopkgtest-pkg-python

Package: python-libnmap
Architecture: all
Depends: ${python:Depends}, ${misc:Depends}, nmap
Suggests: python-libnmap-doc, python-pymongo, python-sqlalchemy, python-boto3
Description: Python 2 NMAP library
 This package contains a Python library enabling Python developers to
 manipulate nmap process and data:
    * automate or schedule nmap scans on a regular basis
    * manipulate nmap scans results to do reporting
    * compare and diff nmap scans to generate graphs
    * batch process scan reports
    * ...
 The lib currently offers the following modules:
    * process: enables you to launch nmap scans
    * parse: enables you to parse nmap reports or scan results (only XML so
      far) from a file, a string,…
    * report: enables you to manipulate a parsed scan result and de/serialize
      scan results in a json format
    * diff: enables you to see what changed between two scans
    * common: contains basic nmap objects like NmapHost and NmapService. It is
      to note that each object can be “diff()ed” with another similar object.
    * plugins: enables you to support datastores for your scan results directly
      in the “NmapReport” object. from report module:
        * mongodb: insert/get/getAll/delete
        * sqlalchemy: insert/get/getAll/delete
        * aws s3: insert/get/getAll/delete (not supported for Python3 since boto
          is not supporting py3)
 .
 This package installs the library for Python 2.

Package: python3-libnmap
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, nmap
Suggests: python-libnmap-doc, python3-pymongo, python3-sqlalchemy, python3-boto3
Description: Python 3 NMAP library
 This package contains a Python library enabling Python developers to
 manipulate nmap process and data:
    * automate or schedule nmap scans on a regular basis
    * manipulate nmap scans results to do reporting
    * compare and diff nmap scans to generate graphs
    * batch process scan reports
    * ...
 The lib currently offers the following modules:
    * process: enables you to launch nmap scans
    * parse: enables you to parse nmap reports or scan results (only XML so
      far) from a file, a string,…
    * report: enables you to manipulate a parsed scan result and de/serialize
      scan results in a json format
    * diff: enables you to see what changed between two scans
    * common: contains basic nmap objects like NmapHost and NmapService. It is
      to note that each object can be “diff()ed” with another similar object.
    * plugins: enables you to support datastores for your scan results directly
      in the “NmapReport” object. from report module:
        * mongodb: insert/get/getAll/delete
        * sqlalchemy: insert/get/getAll/delete
        * aws s3: insert/get/getAll/delete (not supported for Python3 since boto
          is not supporting py3)
 .
 This package installs the library for Python 3.

Package: python-libnmap-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Description: Python NMAP Library (common documentation)
 This package contains a Python library enabling Python developers to
 manipulate nmap process and data:
    * automate or schedule nmap scans on a regular basis
    * manipulate nmap scans results to do reporting
    * compare and diff nmap scans to generate graphs
    * batch process scan reports
    * ...
 The lib currently offers the following modules:
    * process: enables you to launch nmap scans
    * parse: enables you to parse nmap reports or scan results (only XML so
      far) from a file, a string,…
    * report: enables you to manipulate a parsed scan result and de/serialize
      scan results in a json format
    * diff: enables you to see what changed between two scans
    * common: contains basic nmap objects like NmapHost and NmapService. It is
      to note that each object can be “diff()ed” with another similar object.
    * plugins: enables you to support datastores for your scan results directly
      in the “NmapReport” object. from report module:
        * mongodb: insert/get/getAll/delete
        * sqlalchemy: insert/get/getAll/delete
        * aws s3: insert/get/getAll/delete (not supported for Python3 since boto
          is not supporting py3)
 .
 This is the common documentation package.
